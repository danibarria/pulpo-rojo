import type { NextPage } from 'next'
import Head from 'next/head'
import BurgerMenuWhite from '../assets/home/burger-menu-white.svg'
import Header from '../components/Home/Header'
import HeaderDesktop from '../components/Home/Header/Header.desktop'

import Main from '../components/Home/Main'
import Footer from '../components/Home/Footer'
import FooterDesktop from '../components/Home/Footer/Footer.desktop'
import Subheader from '../components/Home/Subheader'
import { useState } from 'react'

const initialMenu = [
  {
      key: 1,
      name: "nosotrxs",
      selected: false,
      subMenu:["▸proyecto","▸cooperativa","▸cerveceria","▸chubut"]
  },
  
  {
      key: 2,
      name: "bares",
      selected: false,
      subMenu: ["▸trelew", "▸pto. madryn", "▸bares donde encontrarnos"]
  },
  
  {
      key: 3,
      name: "cervezas",
      selected: false,
      subMenu:["▸estilos","▸pizarras","▸premios"]
  },
  
  {
      key: 4,
      name: "tienda",
      selected: false,
      subMenu:["▸productos","▸pedidosya","▸alquiler de barriles"]
  },
  
  {
      key: 5,
      name: "blog",
      selected: false
  },

  {
      key:6,
      name: "burguer",
      selected:false,
      subMenu:["proyecto◂","cooperativa◂","cerveza◂","fabrica◂","bares◂","tienda◂","contacto◂"],
      image: BurgerMenuWhite
  }

]


const Home: NextPage = () => {
  const [subMenus, setSubMenus] = useState(initialMenu)


  const handleSelect = (key: number) => {
    setSubMenus(prevSubMenus => prevSubMenus.map(item => (item.key == key? item.selected? {...item, selected:false} :{...item, selected:true}:{...item, selected:false})))
    
  }

  return (
    <div>
      <Head>
        <title>Cooperativa Pulpo Rojo</title>
        <meta name="description" content="Página del pulpo rojo" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header handleSelect={handleSelect} burgerMenu={subMenus[subMenus.length-1]}/>
      <Subheader handleSelect={handleSelect} subMenus={subMenus}/>

      <HeaderDesktop />
      {/* Here is Pulperias, Estilos and Contacto */}
      <Main />
      <Footer />
      <FooterDesktop />
    </div>
  )
}

export default Home
