export const Fonts = {
  Rumbleweed: "Rumbleweed",
  Gobold: "Gobold",
  Veneer: "Veneer",
  Leonid: "Leonid",
};

export const Colors = {
  LowRed: "#680505",
  StrongRed: "#F32836",
  LowBlack: "rgba(0, 0, 0, 0.5)",
  White: "#FFFFFF",
}

const theme = {
    pages: {
      home: {
        header: {
          backgroundColor:'var(--color-low-red)',
          color: 'var(--color-white)',
        }

      }
    }
  }
  
  export default theme