import { createGlobalStyle } from 'styled-components'
import { Colors } from './theme'

export default createGlobalStyle`
  @font-face {
    font-family: 'Gobold';
    src: url('/fonts/Gobold_Bold.ttf');
    font-weight: 'normal';
  }
  @font-face {
    font-family: 'Rumbleweed';
    src: url('/fonts/Rumbleweedspur.ttf');
    font-weight: 'normal';
  }
  @font-face {
    font-family: 'Veneer';
    src: url('/fonts/Veneer.ttf');
  }
  @font-face {
    font-family: 'Leonid';
    src: url('/fonts/Leonid.ttf');
  }

  *{
    margin: 0;
    padding:0;
    box-sizing:border-box;

    --color-low-red: ${Colors.LowRed};
    --color-strong-red: ${Colors.StrongRed};
    --color-low-black: ${Colors.LowBlack};
    --color-white: ${Colors.White};
  }
  html {
    height: 100%;
  }
  body {
    height: 100%;
    background: #241E1E;
  }
  #__next {
  height: 100%;
  }
`