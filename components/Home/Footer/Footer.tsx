import Image from 'next/image'
import * as SC from './style'
import facebookIcon from '../../../assets/footer/facebook.svg'
import instagramIcon from '../../../assets/footer/instagram.svg'

const Footer = () => {
    return (
        <SC.Footer>
            <SC.SocialNetworks style={{ padding: '7px 0 15px 0' }}>
                <SC.SocialItem>
                    <SC.Links href='#'>visitas</SC.Links>
                </SC.SocialItem>
                <SC.SocialItem>
                    <SC.Links href='#'>contacto</SC.Links>
                </SC.SocialItem>
                <SC.SocialItem>
                    <SC.Links href='#'>eventos</SC.Links>
                </SC.SocialItem>
            </SC.SocialNetworks>
            <SC.SocialNetworks style={{ padding: '0 0 25px 0' }}>
                <SC.SocialItem>
                    <Image src={facebookIcon} alt="Facebook" width={18.4} height={18.4} />
                    <SC.SocialItemTitle>pulporojocoop</SC.SocialItemTitle>
                </SC.SocialItem>
                <SC.SocialItem>
                    <Image src={instagramIcon} alt="Instagram" width={18.4} height={18.4} />
                    <SC.SocialItemTitle>cervezapulporojo</SC.SocialItemTitle>
                </SC.SocialItem>

            </SC.SocialNetworks>
            <SC.Title style={{ padding: '0 0 10px 0' }}>Info Legal Pulpo Rojo</SC.Title>
        </SC.Footer>
    )
}

export default Footer