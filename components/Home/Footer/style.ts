import styled from "styled-components";

import { Fonts } from "../../../styles/theme";

export const Footer = styled.footer`
    width: 100%;
    background: black;
    color: ${props => props.theme.pages.home.header.color};

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    // media query only on desktop
    @media (min-width: 768px) {
        display: none;
    }
`

export const Title = styled.h1`
    font-family: ${Fonts.Veneer};
    font-size: 16px;
    padding-left: 5px;
`

export const LogoWrapper = styled.div`
    width: 48px;
    & > svg {
        fill: white;
        color: white;
    }
`

export const SocialNetworks = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    width: 100%;
`

export const SocialItem = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`

export const SocialItemTitle = styled.h3`
    text-transform: uppercase;
    font-family: ${Fonts.Gobold};
    font-size: 16px;
    font-weight: 400;
    margin-left: 1.6px;
`

export const Links = styled.a`
    text-decoration: underline;
    font-family: ${Fonts.Veneer};
    font-size: 16px;
    color: white;
`