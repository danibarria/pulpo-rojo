import Image from 'next/image'
import * as SC from './style.desktop'
import facebookIcon from '../../../assets/footer/facebook.svg'
import instagramIcon from '../../../assets/footer/instagram.svg'

const Footer = () => {
    return (
        <SC.Wrapper>
        <SC.Footer>
                <SC.SocialItem>
                    <SC.Links href='#'>visitas</SC.Links>
                </SC.SocialItem>
                <SC.SocialItem>
                    <SC.Links href='#'>contacto</SC.Links>
                </SC.SocialItem>
                <SC.SocialItem>
                    <SC.Links href='#'>eventos</SC.Links>
                </SC.SocialItem>

                <SC.SocialItem>
                    <Image src={facebookIcon} alt="Facebook" width={32.02} height={32.02} />
                    <SC.SocialItemTitle>pulporojocoop</SC.SocialItemTitle>
                </SC.SocialItem>
                <SC.SocialItem>
                    <Image src={instagramIcon} alt="Instagram" width={32.02} height={32.02} />
                    <SC.SocialItemTitle>cervezapulporojo</SC.SocialItemTitle>
                </SC.SocialItem>

        </SC.Footer>
        <SC.Title style={{ padding: '100px 0 10px 0' }}>Info Legal Pulpo Rojo</SC.Title>
        </SC.Wrapper>
    )
}

export default Footer