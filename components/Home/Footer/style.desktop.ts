import styled from "styled-components";

import { Fonts } from "../../../styles/theme";

export const Wrapper = styled.footer`
    width: 100%;
    background: black;
    color: ${props => props.theme.pages.home.header.color};

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 16px 0 0;
    @media (max-width: 768px) {
        display: none;
    }
`

export const Footer = styled.div`
    width: 100%;
    background: black;
    color: ${props => props.theme.pages.home.header.color};

    display: flex;
    justify-content: center;
    align-items: center;
`

export const Title = styled.h1`
    font-family: ${Fonts.Veneer};
    font-size: 28px;
    font-style: italic;
    font-weight: 400;
    line-height: 29px;
    letter-spacing: 0em;
    text-align: center;

`

export const LogoWrapper = styled.div`
    width: 48px;
    & > svg {
        fill: white;
        color: white;
    }
`

export const SocialNetworks = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    width: 100%;
`

export const SocialItem = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;

    margin-right: 50px;
`

export const SocialItemTitle = styled.h3`
    text-transform: uppercase;
    font-family: ${Fonts.Gobold};
    font-size: 30px;
    font-weight: 400;
    line-height: 39px;
    letter-spacing: 0em;
    text-align: left;

    margin-left: 10px;

`

export const Links = styled.a`
    text-decoration: underline;
    font-family: ${Fonts.Veneer};
    color: white;

    font-size: 30px;
    font-weight: 400;
    letter-spacing: 0em;
    text-align: center;

    padding-top: 5px;

`