import Image from 'next/image'
import * as SC from './style.desktop'
import Logo from '../../../assets/home/new-logo.svg'

const LEFT_MENU_ITEMS = [
    {
        label: 'cooperativa',
        url: '#',
    },
    {
        label: 'fábrica',
        url: '#',
    },
    {
        label: 'trelew',
        url: '#',
    },
    {
        label: 'puerto madryn',
        url: '#',
    },
]

const RIGHT_MENU_ITEMS = [
    {
        label: 'cervezas',
        url: '#',
    },
    {
        label: 'bares',
        url: '#',
    },
    {
        label: 'tienda',
        url: '#',
    },
    {
        label: 'contacto',
        url: '#',
    },
    {
        label: 'proyecto',
        url: '#',
    },
    {
        label: 'blog',
        url: '#',
    },
]

const Header = () => {
    
    
    return (
        <SC.Header>
            <SC.SpacedEvenly>
                {LEFT_MENU_ITEMS.map( (item:any) => (
                    <SC.Link key={item}>
                        <a href={item.url}>{item.label}</a>
                    </SC.Link>
                ))}
            </SC.SpacedEvenly>
            <SC.SpacedCenter>
                <SC.LogoWrapper>
                    <Image src={Logo} width={"50px"} height={"40px"} alt={"Pulpo Logo"} />
                </SC.LogoWrapper>
                <SC.Title>Cerveza pulpo rojo</SC.Title>
            </SC.SpacedCenter>
            <SC.SpacedEvenly>
                {RIGHT_MENU_ITEMS.map( (item:any) => (
                    <SC.Link key={item}>
                        <a href={item.url}>{item.label}</a>
                    </SC.Link>
                ))}
            </SC.SpacedEvenly>
        </SC.Header>
    )
}

export default Header