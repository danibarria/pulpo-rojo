import Image from 'next/image'
import * as SC from './style'
import Logo from '../../../assets/home/new-logo.svg'
import WhiteBurger from '../../../assets/home/burger-menu-white.svg'
import BlackBurger from '../../../assets/home/burger-menu-black.svg'

const Header = ({handleSelect, burgerMenu}:{handleSelect:Function, burgerMenu:any}) => {
    
    const burgerImage = burgerMenu.selected ? BlackBurger : WhiteBurger
    
    return (
        <SC.Header>
            <SC.LogoWrapper>
                <Image src={Logo} width={"50px"} height={"40px"} alt={"Pulpo Logo"} />
            </SC.LogoWrapper>
            <SC.Title>Cerveza pulpo rojo</SC.Title>
            <SC.BurgerMenu onClick={()=>handleSelect(burgerMenu.key)}>
                <Image src={burgerImage} width="27" height="20" alt='Menu hamburguesa'/> 
                {/* este es el submenu */ }
                    {
                        burgerMenu.selected && <SC.BurgerSubMenu>
                            <SC.StyledUl>
                                {burgerMenu.subMenu.map((subItem:any) => <SC.StyledLi key={subItem}><span>{subItem}</span></SC.StyledLi>)}
                            </SC.StyledUl>
                        </SC.BurgerSubMenu>
                    }
            </SC.BurgerMenu>
        </SC.Header>
    )
}

export default Header