import styled from "styled-components";

import { Fonts } from "../../../styles/theme";

export const Header = styled.nav`
    width: 100%;
    background: ${props => props.theme.pages.home.header.backgroundColor};
    color: ${props => props.theme.pages.home.header.color};
    height: 47px;
    display: grid;
    grid-template-columns: 1fr 300px 1fr;
    align-items: center;
    justify-items: center;

    // hide on mobile
    @media (max-width: 768px) {
        display: none;
    }
`

export const Title = styled.h1`
    font-family: ${Fonts.Leonid};
    font-size: 27px;
    padding-left: 5px;
    letter-spacing: 0.065em;
`

export const LogoWrapper = styled.div`

    display: flex;
    justify-self: end;
    align-self: end;
    & > img{
        width: 50px;
        height: 40px;
    }

    & > svg {
        fill: white;
        color: white;
    }
`

export const Link = styled.div`
    font-family: Veneer;
    font-size: 20px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0em;
    text-align: center;

    padding-top: 5px;

    color: white;
    & > a {
        color: white;
        text-decoration: none;
    }
`

export const SpacedEvenly = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    align-items: center;
`

export const SpacedCenter = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`