import styled from "styled-components";

import { Fonts } from "../../../styles/theme";

export const Header = styled.nav`
    width: 100%;
    background: ${props => props.theme.pages.home.header.backgroundColor};
    color: ${props => props.theme.pages.home.header.color};
    height: 47px;
    display: grid;
    grid-template-columns: 64px 1fr 42px;
    align-items: center;
    justify-items: center;

    @media (min-width: 768px) {
        display: none;
    }
`

export const Title = styled.h1`
    font-family: ${Fonts.Leonid};
    font-size: 27px;
/*     padding-left: 5px; */
    letter-spacing: 0.065em;
`

export const LogoWrapper = styled.div`

    display: flex;
    justify-self: end;
    align-self: end;
    & > img{
        width: 50px;
        height: 40px;
    }

    & > svg {
        fill: white;
        color: white;
    }
`

export const BurgerMenu = styled.div/* <{source: string}> */`
    position: relative;
    display: flex;
    justify-self: start;
    align-self: center;

    @media (min-width: 768px) {
        display: none;
    }
`

export const BurgerSubMenu = styled.div`
    position: absolute;
    bottom: -175px;
    height: 165px;
    left: -105px;
    padding: 0;
    line-height: 18px;
    width: 131px;
    border-radius: 2px;
    background: rgba(0, 0, 0, 0.65);
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    backdrop-filter: blur(4px);
    z-index: 2;
    font-size: 16px;
    text-align: right;
    font-family: ${Fonts.Veneer};

`
export const StyledUl = styled.ul`
    padding: 5px 5px;
    list-style: none;
`

export const StyledLi = styled.li`
    word-break: break-all;
    color:white;
    letter-spacing: 1px;
    word-spacing: 2px;
    margin: 1px 0;
    padding: 1px 0;
    list-style: none;
`