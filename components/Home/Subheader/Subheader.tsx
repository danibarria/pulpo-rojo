import Image from 'next/image'
import * as SC from './style'
import Logo from '../../../assets/home/white-logo.svg'

import { useState } from 'react'



const Subheader = ({handleSelect, subMenus}:{handleSelect:Function, subMenus:any} ) => {

    return (
        <SC.Header>
            {subMenus.map( (item:any) => ( !(item.key == 6) &&
                <SC.Item key={item} selected={item.selected} onClick={()=>handleSelect(item.key)}>
                    {item.name}
                    {/* este es el submenu */ }
                    {
                        item.subMenu && item.selected && <SC.SubMenu open={item.selected}>
                            <SC.StyledUl>
                                {item.subMenu.map((subItem:any) => <SC.StyledLi key={subItem}><span>{subItem}</span></SC.StyledLi>)}
                            </SC.StyledUl>
                        </SC.SubMenu>
                    }
                       
                    
                </SC.Item>
            ))}
        </SC.Header>
    )
}

export default Subheader