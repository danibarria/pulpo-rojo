import styled from "styled-components";
import { Fonts } from "../../../styles/theme";

export const Header = styled.nav`
    position: relative;
    width: 100%;
    background: ${props => props.theme.pages.home.header.backgroundColor};
    color: ${props => props.theme.pages.home.header.color};
    height: 35.25px;
    display: flex;
    align-items: center;
    justify-content: space-evenly;

    @media (min-width: 768px) {
        display: none;
    }
`

export const BurgerMenu = styled.div/* <{source: string}> */`
    position: absolute;
    margin: 0 0 0 40px;
    left: 78%;
    top: -105%;
    
    
`

export const BurgerSubMenu = styled.div`

    position: absolute;
    bottom: -715%;
    height: 165px;
    left: -105px;
    padding: 0;
    line-height: 18px;
    width: 131px;
    border-radius: 2px;
    background: rgba(0, 0, 0, 0.65);
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    backdrop-filter: blur(4px);
    z-index: 2;
    font-size: 16px;
    text-align: right;
    font-family: ${Fonts.Veneer};

`

export const Item = styled.div<{selected: boolean}>`
    position: relative;
    font-family: ${Fonts.Veneer};
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 24px;
    color: #FFFFFF;
    text-transform: uppercase;
    

    ${({ selected }) => selected && `
        color: black;
    `}
`

export const SubMenu = styled.div<{open: boolean}>`
    position: absolute;
    bottom: -150px;
    height: 146px;
    left: -15px;
    padding-left: 0;
    line-height: 24px;
    width: 131px;
    border-radius: 2px;
    background: rgba(0, 0, 0, 0.65);
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    backdrop-filter: blur(4px);
    z-index: 2;
    font-size: 20px;
`


export const StyledUl = styled.ul` 
    padding: 5px 5px;
    list-style: none;
    
`

export const StyledLi = styled.li`
    word-break: break-all;
    color:white;
    letter-spacing: 1px;
    word-spacing: 2px;
    margin: 1px 0;
    padding: 1px 0;
    list-style: none;
`