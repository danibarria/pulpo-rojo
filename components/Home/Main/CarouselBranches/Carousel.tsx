import React, { useCallback, useEffect, useState } from 'react'
import useEmblaCarousel from 'embla-carousel-react'
import Autoplay from 'embla-carousel-autoplay'
import Image from 'next/image'

import * as SC from './style'
import { DotButton, PrevButton, NextButton } from './CarouselNavigation'

const options = { delay: 4000 } // Options for autoplay
const autoplayRoot = (emblaRoot: any) => emblaRoot.parentElement
const autoplay = Autoplay(options, autoplayRoot)

const EmblaCarousel = () => {
  const [emblaRef, embla] = useEmblaCarousel({ loop: false }, [autoplay])
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [scrollSnaps, setScrollSnaps] = useState<number[]>([]);
  
  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const scrollTo = useCallback((index: number) => embla && embla.scrollTo(index), [
    embla
  ]);

  const onSelect = useCallback(() => {
    if (!embla) return;
    setSelectedIndex(embla.selectedScrollSnap());
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla, setSelectedIndex]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    setScrollSnaps(embla.scrollSnapList());
    embla.on("select", onSelect);
  }, [embla, setScrollSnaps, onSelect]);

  return (
    <SC.Wrapper>
      <div className="embla">
        <div className="embla__viewport" ref={emblaRef}>

            <div className="embla__container">
              <SC.Slide key={`Slide-4`}>
                <Image src="/images/sucursal-fabrica-trelew.desktop.png" width={"1440px"} height={"560px"} alt="Preview" layout="responsive"/>
              </SC.Slide>
              <SC.Slide key={`Slide-4`}>
                <Image src="/images/sucursal-fabrica-trelew.desktop.png" width={"1440px"} height={"560px"} alt="Preview" layout="responsive"/>
              </SC.Slide>
              <SC.Slide key={`Slide-4`}>
                <Image src="/images/sucursal-fabrica-trelew.desktop.png" width={"1440px"} height={"560px"} alt="Preview" layout="responsive"/>
              </SC.Slide>
            </div>
        </div>
      </div>

      <SC.DotsWrapper className="embla__dots">
        {scrollSnaps.map((_, index) => (
          <DotButton
            key={index}
            selected={index === selectedIndex}
            onClick={() => scrollTo(index)}
          />
        ))}
      </SC.DotsWrapper>
    </SC.Wrapper>
  )
}

export default EmblaCarousel;