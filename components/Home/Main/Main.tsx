import Card from '../../Card'
import * as SC from './style'
import CarouselNews from './CarouselNews'
import CarouselBranches from './CarouselBranches'
import CarouselBeers from './CarouselBeers'

const LINKS = [
    {
        label: 'bares donde encontrarnos',
        url: '#',
    },
    {
        label: 'tienda',
        url: '#',
    },
    {
        label: 'pedidosya',
        url: '#',
    },
    {
        label: 'contacto',
        url: '#',
    },
]

const Main = () => {
    return (
        <SC.Main>
            <SC.CustomText2>CERVEZA ARTESANAL CHUBUTENSE</SC.CustomText2>
            <CarouselBranches />
            <SC.SecondaryTitle lineHeight='25.2px' margin='11px auto 0'>
                Nuestras Cervezas
            </SC.SecondaryTitle>
            <CarouselBeers />
            <SC.Links>
                ver mas
            </SC.Links>
            <CarouselNews />


                <Card
                    background='rgba(118, 4, 3, 0.86);'
                    padding='11px 0 11px'
                    color='white'
                    margin="18px 20px 20px"
                    width='auto'
                    hideOnDesktop
                >
                    <SC.CardInfo>
                        <h3>bares donde encontrarnos</h3>
                    </SC.CardInfo>
                </Card>

                <SC.LinksWrapper>
                {LINKS.map((link) => (
                    <Card
                        background='rgba(118, 4, 3, 0.86);'
                        padding='11px 0 5px'
                        color='white'
                        margin="18px 20px 20px"
                        width='320px'
                        hideOnDesktop={false}
                        key={link.label}>
                        <h4><a href={link.url} target="_blank" rel="noopener noreferrer">{link.label}</a></h4>
                    </Card>
                ))}
                </SC.LinksWrapper>

            <Card
                background='rgba(0, 0, 0, 0.65)'
                padding='62px 20px'
                color='white'
                margin="28px 0 115px"
                hideOnDesktop={false}
            >
                <SC.CardInfo>
                    <SC.CustomText>{`"`}EL PROYECTO{`"`}</SC.CustomText>
                    <SC.CustomText>(TEXTO)</SC.CustomText>
                </SC.CardInfo>
            </Card>

        </SC.Main>

    )
}

export default Main