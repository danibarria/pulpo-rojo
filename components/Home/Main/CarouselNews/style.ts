import styled from "styled-components";

export const Wrapper = styled.div`
  overflow: hidden;
  margin-top: 23px;
  padding: 0 20px;
  position: relative;

  .embla {
    position: relative;
    background-color: transparent;
    padding: 0px;
    max-width: 670px;
    margin-left: auto;
    margin-right: auto;

    @media (min-width: 768px) {
      max-width: calc(100% - 78px - 48px);
    }
    
  }
  
  .embla__viewport {
    overflow: hidden;
    width: 100%;
  }
  
  .embla__viewport.is-draggable {
    cursor: move;
    cursor: grab;
  }
  
  .embla__viewport.is-dragging {
    cursor: grabbing;
  }
  
  .embla__container {
    display: flex;
    user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -webkit-tap-highlight-color: transparent;
    margin-left: 2px;
  }
  
  .embla__slide {
    position: relative;
    min-width: 100%;
    padding-left: 10px;
  }
  
  .embla__slide__inner {
    position: relative;
    overflow: hidden;
    height: 190px;
  }
  
  .embla__slide__img {
    position: absolute;
    display: block;
    top: 50%;
    left: 50%;
    width: auto;
    min-height: 100%;
    min-width: 100%;
    max-width: none;
    transform: translate(-50%, -50%);
  }
  
  .embla__button {
    outline: 0;
    cursor: pointer;
    background-color: transparent;
    touch-action: manipulation;
    position: absolute;
    z-index: 1;
    top: 50%;
    transform: translateY(-50%);
    border: 0;
    justify-content: center;
    align-items: center;
    fill: white;
    padding: 0;
  }
  
  .embla__button:disabled {
    cursor: default;
    opacity: 0.3;
  }
  
  .embla__button__svg {
    width: 100%;
    height: 100%;
  }
  
  .embla__button--prev {
    left: -20px;
    @media (min-width: 768px) {
      left: -78px;
    }
  }
  
  .embla__button--next {
    right: -20px;
    @media (min-width: 768px) {
      right: -78px;
    }
  }
    
      
`
export const Container = styled.div`
    display: flex;
    position: relative;
    background-color: #f7f7f7;
    padding: 20px;
    margin-left: auto;
    margin-right: auto;
`
export const Slide = styled.div`
    position: relative;
    flex: 0 0 100%;
`

export const DotsWrapper = styled.div`
    position:absolute;
    z-index:1;
    left: 47%;
    top: 75%;
    display: flex;
    list-style: none;
    justify-content: center;
    padding: 10px 0 5px;

    column-gap: 5px;
    
    @media (min-width: 768px) {
      bottom: 20px;
      align-items: flex-end;
      column-gap: 10px;
    }
`

export const Dot = styled.button<{ selected: boolean }>`
    border: none;
    border-radius: 50%;
    width: 7px;
    height: 7px;
    background-color: var(--color-white);

    ${({ selected }) => selected && `
        box-shadow: 0 0 0 1px white;
        background-color: var(--color-white);
    `}

    @media (min-width: 768px) {
      width: 23px;
      height: 23px;
    }
`

export const Button = styled.button<{ inverted: boolean }>`
    outline: 0;
    cursor: pointer;
    background-color: transparent;
    touch-action: manipulation;
    position: absolute;
    z-index: 1;
    top: 50%;
    transform: translateY(-50%);
    border: 0;
    width: 21px;
    height: 21px;
    justify-content: center;
    align-items: center;
    fill: #1bcacd;
    padding: 0;
   
    @media (min-width: 768px) {
      width: 78px;
      height: 78px;
    }
`