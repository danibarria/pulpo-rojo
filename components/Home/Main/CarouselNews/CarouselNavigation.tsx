import React, { MouseEventHandler } from "react";
import * as SC from "./style";

export const DotButton = ({ selected, onClick }: { selected: boolean, onClick: MouseEventHandler<HTMLButtonElement>}) => (
  <SC.Dot
    className={`embla__dot ${selected ? "is-selected" : ""}`}
    type="button"
    onClick={onClick}
    selected={selected}
  />
);

export const PrevButton = ({ enabled, onClick }: { enabled: boolean, onClick: MouseEventHandler<HTMLButtonElement>}) => (
  <SC.Button
    className="embla__button embla__button--prev"
    onClick={onClick}
    disabled={!enabled}
    inverted
  >
    <svg className="embla__button__svg" width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_401_61)">
    <path d="M49.0135 57.973L29.5135 38.473L49.0135 18.973" stroke="white" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round"/>
    </g>
    <defs>
    <filter id="filter0_d_401_61" x="24.5135" y="14.973" width="29.5" height="49" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
    <feOffset dy="1"/>
    <feGaussianBlur stdDeviation="1.5"/>
    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6 0"/>
    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_401_61"/>
    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_401_61" result="shape"/>
    </filter>
    </defs>
    </svg>
  </SC.Button>
);

export const NextButton = ({ enabled, onClick }: { enabled: boolean, onClick: MouseEventHandler<HTMLButtonElement>}) => (
  <SC.Button
    className="embla__button embla__button--next"
    onClick={onClick}
    disabled={!enabled}
    inverted/* ={false} */
  >
    <svg className="embla__button__svg" width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_401_59)">
    <path d="M29.25 58.5L48.75 39L29.25 19.5" stroke="white" strokeWidth="4" strokeLinecap="round" strokeLinejoin="round"/>
    </g>
    <defs>
    <filter id="filter0_d_401_59" x="24.25" y="15.5" width="29.5" height="49" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
    <feOffset dy="1"/>
    <feGaussianBlur stdDeviation="1.5"/>
    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6 0"/>
    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_401_59"/>
    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_401_59" result="shape"/>
    </filter>
    </defs>
    </svg>
  </SC.Button>
);