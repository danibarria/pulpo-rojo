import React, { useCallback, useEffect, useState } from 'react'
import useEmblaCarousel from 'embla-carousel-react'
import Autoplay from 'embla-carousel-autoplay'
import Image from 'next/image'

import * as SC from './style'
import { PrevButton, NextButton } from './CarouselNavigation'

const options = { delay: 4000 } // Options for autoplay
const autoplayRoot = (emblaRoot: any) => emblaRoot.parentElement
const autoplay = Autoplay(options, autoplayRoot)

const EmblaCarousel = () => {
  const [emblaRef, embla] = useEmblaCarousel({ 
    align: 0.04,
    slidesToScroll: 4,
    loop: true,
  }, [autoplay])
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);
  
  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);


  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    embla.on("select", onSelect);
  }, [embla, onSelect]);

  return (
    <SC.Wrapper>
      <div className="embla">
        <div className="embla__viewport" ref={emblaRef}>
            <div className="embla__container">
                {[...Array(8)].map((_, i) => (
                  <SC.Slide key={`Slide-${i}`}>
                    <Image
                      src="/images/beers.desktop.png"
                      width={"327px"}
                      height={"234px"}
                      layout="responsive"
                      alt="Preview"
                    />
                  </SC.Slide>
                ))}
            </div>
        </div>
        <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
        <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
      </div>
    </SC.Wrapper>
  )
}

export default EmblaCarousel;