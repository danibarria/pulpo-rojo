import styled from "styled-components";
import { Fonts } from "../../../styles/theme";

export const Main = styled.main`
    padding: 0;
    display: flex;
    flex-direction: column;
    overflow:hidden;
`

{/* PULPERÍAS
    Trelew - Puerto Madryn
    17:00 - 22:00 hs */}
export const CardInfo = styled.article`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-family: ${Fonts.Gobold};
    font-size: 16px;
    text-align: center;
    text-shadow: 0px 3px 4px rgba(0, 0, 0, 0.7);
`



export const TwoColumns = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 12px;
`

export const SecondaryTitle = styled.h2<{ color?: string, fontSize?: string, lineHeight?: string, margin?: string}>`
    font-family: ${Fonts.Veneer};
    font-size: ${props => props.fontSize || '21px'};
    line-height: ${props => props.lineHeight || '25px'};
    color: ${props => props.color || 'white'};
    margin: ${props => props.margin || '0 auto'};

    @media (min-width: 768px) {
        font-size: 50px;
        font-weight: 400;
        line-height: 60px;
        letter-spacing: 0em;
        text-align: center;
    }
`

export const TertiaryTitle = styled.h3<{ color?: string, fontSize?: string, lineHeight?: string}>`
    font-family: ${Fonts.Veneer};
    font-size: ${props => props.fontSize || '12px'};
    line-height:  ${props => props.lineHeight || '14px'};
    color: ${props => props.color || 'white'};
    text-align: center;
    text-decoration: underline;
`

export const ImageContainer = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 100%;
    margin: 10px 0 14px;
`

export const Links = styled.a`
    text-decoration: underline;
    font-family: ${Fonts.Veneer};
    font-size: 12px;
    line-height: 14px;
    text-align: center;
    color: white;

    @media (min-width: 768px) {
        font-family: Veneer;
        font-size: 30px;
        font-weight: 400;
        line-height: 36px;
        letter-spacing: 0em;
        text-align: center;

    }
`

export const CustomText = styled.h3`
    font-family: ${Fonts.Veneer};
    font-size: 20px;
    font-weight: 400;
    letter-spacing: 0em;
    text-align: center;

    @media (min-width: 768px) {
        font-size: 60px;
        font-weight: 400;
        line-height: 72px;
    }
`

export const CustomText2 = styled.h4`
    font-family: Leonid;
    font-size: 52px;
    font-weight: 400;
    line-height: 43px;
    letter-spacing: 0.065em;
    text-align: center;
    color: white;

    margin: 23px auto;

    @media (max-width: 768px) {
        display: none;
    }
`

export const LinksWrapper = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 100%;
    // hide on desktop
    @media (max-width: 768px) {
        display: none;
    }
    
`