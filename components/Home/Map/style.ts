import styled from "styled-components";
import { Fonts } from "../../../styles/theme";


export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const MapButton = styled.button<{ selected: boolean }>`
    font-family: ${Fonts.Gobold};
    letter-spacing: 1px;
    color: var(--color-strong-red);
    background-color: var(--color-white);
    border: none;
    padding: 10px 5px;
    border-radius: 5px;
    margin: 5px;
    width: 200px;

    ${props => props.selected && `
        background-color: var(--color-white);
        color: var(--color-low-black);
    `}
`;

export const Small = styled.small`
    font-style: italic;
    font-size: 8px;
`