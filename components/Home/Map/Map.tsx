import React, { useState } from 'react'
import Card from '../../Card'

import * as SC from './style'
import * as MSC from '../Main/style'

interface PuntoPulpo {
  title: string
  query: string
}

const PuntoPulpoTrelew: PuntoPulpo = { title: "Punto Pulpo, Trelew", query: "Punto+Pulpo,Trelew" }
const CooperativaPulpoTrelew: PuntoPulpo = { title: "Cooperativa Pulpo Rojo, Trelew", query: "Cooperativa+Pulpo,Trelew" }
const PulpoBarMadryn: PuntoPulpo = { title: "Pulpo Bar, Puerto Madryn", query: "Pulpo+Bar,Puerto+Madryn" }

const Map = () => {
  const [selectedMap, setSelectedMap] = useState<PuntoPulpo>(PuntoPulpoTrelew)

  return (
    <SC.Wrapper>
      <Card
        background='rgba(0, 0, 0, 0.65)'
        padding='18px'
        margin='30px 15px 10px'
        color='white'
        hideOnDesktop={false}
      >
        <MSC.CardInfo>
          <MSC.SecondaryTitle
            lineHeight='32.76px'
            fontSize='25px'
          >
            VISITANOS EN NUESTROS PUNTOS DE ENCUENTRO
          </MSC.SecondaryTitle>
        </MSC.CardInfo>
      </Card>

      {[PuntoPulpoTrelew, CooperativaPulpoTrelew, PulpoBarMadryn].map((map: PuntoPulpo, index) => (
        <SC.MapButton
          key={index}
          onClick={() => setSelectedMap(map)}
          selected={selectedMap === map}
        >
          {map.title}{' '}
          {selectedMap === map &&
            <SC.Small>Seleccionado</SC.Small>
          }
        </SC.MapButton>
      ))}

      <iframe
        width="300"
        height="300"
        loading="lazy"
        allowFullScreen={true}
        referrerPolicy="no-referrer-when-downgrade"
        style={{ border: 'none', width: '100%', marginTop: '15px' }}
        src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyD9e0_XM_89fsD83xj4yaPXNvjlGJATrEQ
                &q=${selectedMap.query}`}>
      </iframe>
    </SC.Wrapper>
  )
}

export default Map