import * as SC from './style'

const Card = ({
    background,
    color,
    padding,
    children,
    width,
    margin,
    hideOnDesktop,
}: { background: string, color: string, padding: string, children: JSX.Element, hideOnDesktop: boolean, margin?:string, width?: string }) => {
    return (
        <SC.Wrapper background={background} color={color} padding={padding} margin={margin} width={width} hideOnDesktop={hideOnDesktop}>
            {children}
        </SC.Wrapper>
    )
}

export default Card