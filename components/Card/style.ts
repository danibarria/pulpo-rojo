import styled from "styled-components";
import { Fonts } from "../../styles/theme";


export const Wrapper = styled.article<{
    background: string,
    color: string,
    padding: string,
    hideOnDesktop: boolean,
    margin?: string,
    width?: string
}>`
    border-radius: 2px;
    width: ${ props => props.width || "100%"};
    background: ${props => props.background};
    color: ${props => props.color};
    padding: ${props => props.padding};
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    margin: ${props => props.margin || "0"};
    backdrop-filter: blur(4px);

    @media (min-width: 768px) {
        ${props => props.hideOnDesktop ? "display: none;" : ""};
        
        & > h4  {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        & > h4 > a {
            font-family: ${Fonts.Veneer};
            text-decoration: none;
            color: white;
            text-transform: uppercase;

            font-size: 28px;
            font-weight: 400;
            line-height: 34px;
            letter-spacing: 0em;
            text-align: center;

        }
    }
`